/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_randint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 04:04:48 by adequidt          #+#    #+#             */
/*   Updated: 2018/12/12 15:27:48 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

int		ft_randint(void)
{
	unsigned int	bleh;
	unsigned int	i;
	void			*nbr;

	nbr = malloc(1);
	bleh = (unsigned int)nbr;
	bleh = bleh / 10;
	i = (bleh % 10) * 10;
	bleh = bleh / 10;
	i += bleh % 10;
	return ((int)i);
}
