/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 16:47:23 by adequidt          #+#    #+#             */
/*   Updated: 2018/12/11 13:50:06 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(const char *s)
{
	int		bob;
	char	*copy;

	bob = 0;
	while (s[bob])
		bob++;
	if (!(copy = malloc(sizeof(char) * (bob + 1))))
		return (NULL);
	bob = 0;
	while (s[bob])
	{
		copy[bob] = s[bob];
		bob++;
	}
	copy[bob] = '\0';
	return (copy);
}
