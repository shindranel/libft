/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/28 19:14:47 by adequidt          #+#    #+#             */
/*   Updated: 2018/12/11 15:20:53 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*link;
	t_list	*next_link;

	if (del == NULL || alst == NULL || *alst == NULL)
		return ;
	link = *alst;
	next_link = link;
	while (next_link)
	{
		del(link->content, link->content_size);
		next_link = link->next;
		free(link);
		link = next_link;
	}
	*alst = NULL;
}
