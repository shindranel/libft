/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/28 19:58:48 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/19 22:36:41 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new_list_start;
	t_list	*new_link;

	if (!(new_list_start = f(tf_lstclone(lst))))
		return (NULL);
	lst = lst->next;
	new_link = f(tf_lstclone(lst));
	new_list_start->next = new_link;
	while (lst->next)
	{
		lst = lst->next;
		if (!(new_link->next = f(tf_lstclone(lst))))
			return (NULL);
		new_link = new_link->next;
	}
	return (new_list_start);
}
