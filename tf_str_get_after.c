/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tf_str_get_after.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 13:10:54 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/19 22:33:47 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*tf_str_get_after(char *str, char end)
{
	int		i;
	char	*cpy;

	if (!(end))
	{
		ft_putendl("Arguments attendus: str, separateur");
		return (0);
	}
	i = 0;
	while (str[i] != end && str[i])
	{
		i++;
	}
	if (!(str[i]))
		return (0);
	cpy = tf_strdup(str + i + 1);
	return (cpy);
}
