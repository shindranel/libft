/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tf_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 17:08:41 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/20 02:43:19 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int	count_words(char const *s, char c)
{
	int		i;
	int		trig;

	i = 0;
	trig = 1;
	while (*s == c && *s)
		s++;
	while (*s)
	{
		if (*s != c && trig == 1)
		{
			i++;
			trig = 0;
		}
		if (*s == c && trig == 0)
		{
			trig = 1;
		}
		s++;
	}
	return (i);
}

static int	word_len(char const *s, char c)
{
	int		len;

	len = 0;
	while (*s != c && *s)
	{
		len++;
		s++;
	}
	return (len);
}

static char	*find_word(char const *s, int len)
{
	char	*word;
	int		i;

	i = 0;
	if (!(word = malloc(sizeof(char) * (len + 1))))
		return (NULL);
	while (i < len)
	{
		word[i] = s[i];
		i++;
	}
	word[i] = '\0';
	return (word);
}

char		**tf_strsplit(char const *s, char c)
{
	int		i;
	int		index;
	char	**tab;

	if (!s || !c)
	{
		return (0);
	}
	i = count_words(s, c);
	if (!(tab = malloc(sizeof(char*) * (i + 1))))
		return (NULL);
	index = 0;
	while (i--)
	{
		while (*s == c && *s)
			s++;
		if (!(tab[index] = find_word(s, word_len(s, c))))
			return (NULL);
		s += word_len(s, c);
		index++;
	}
	tab[index] = NULL;
	return (tab);
}
