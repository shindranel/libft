/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dice.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 04:34:57 by adequidt          #+#    #+#             */
/*   Updated: 2018/12/12 15:25:01 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_dice(int type)
{
	int		result;

	result = ft_randint();
	while (result > type || result == 0)
	{
		result = ft_randint();
	}
	return (result);
}
