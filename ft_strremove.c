/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strremove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/18 21:52:24 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/19 03:16:13 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Fonction permettant de supprimer une partie d'un string.
** Si opt est mis a 0, la fonction supprimera tout a partir de del
** Sinon, elle recuperera toute la string a l'exception de del
*/

char	*ft_strremove(char *str, char *del, int opt)
{
	char	*rest;
	int		len;
	int		j;
	int		i;

	i = 0;
	j = 0;
	len = ft_strlen(str) - ft_strlen(del);
	if (!(rest = malloc(sizeof(char) * (len + 1))))
		exit(0);
	while (ft_partcmp(str + i, del))
	{
		rest[i] = str[i];
		i++;
	}
	if (opt)
	{
		while (i != len)
		{
			rest[i] = str[i + ft_strlen(del)];
			i++;
		}
	}
	rest[i] = '\0';
	return (rest);
}
