/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tf_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 18:09:16 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/19 22:33:17 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int	malloc_calc(int n)
{
	int		i;
	int		j;

	j = n;
	i = 1;
	while ((j /= 10) != 0)
		i++;
	if (n < 0)
		i++;
	return (i);
}

char		*tf_itoa(int n)
{
	char	*str;
	int		i;
	int		j;

	i = malloc_calc(n);
	if (!(str = malloc(sizeof(char) * (i + 1))))
		return (NULL);
	if (n == 0)
		return (tf_strdup("0"));
	j = n;
	str[i--] = '\0';
	if (n < 0)
	{
		str[0] = '-';
		str[i--] = (-(j % 10)) + '0';
		j = -(j / 10);
	}
	while (j)
	{
		str[i--] = (j % 10) + '0';
		j /= 10;
	}
	return (str);
}
