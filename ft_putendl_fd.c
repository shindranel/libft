/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 15:43:05 by adequidt          #+#    #+#             */
/*   Updated: 2018/11/12 15:45:07 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putendl_fd(char const *s, int fd)
{
	int		bob;

	bob = 0;
	while (s[bob])
	{
		ft_putchar_fd(s[bob], fd);
		bob++;
	}
	ft_putchar_fd('\n', fd);
}
