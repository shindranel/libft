/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tf_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 16:46:23 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/19 22:47:01 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int	cut_count(char const *s)
{
	int		i;

	i = 0;
	while (s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
		i++;
	return (i);
}

char		*tf_strtrim(char const *s)
{
	int		i;
	int		j;
	int		k;
	char	*str;
	char	*rev_str;

	if (!s || !(rev_str = tf_strrev((char*)s)))
		return (NULL);
	j = ft_strlen(s);
	j -= (cut_count((char *)s) + cut_count(rev_str));
	j = (j > 0 ? j : 0);
	if (!(str = malloc(sizeof(char) * (j + 1))))
		return (NULL);
	i = 0;
	while (s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
		i++;
	k = 0;
	while (j-- > 0)
	{
		str[k] = s[i];
		i++;
		k++;
	}
	str[k] = '\0';
	return (str);
}
