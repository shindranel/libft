/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_array_clean.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/20 01:16:11 by adequidt          #+#    #+#             */
/*   Updated: 2019/07/13 23:57:02 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

void		ft_array_clean(char **array)
{
	int		i;

	if (!(array))
		return ;
	i = 0;
	while (array[i])
		i++;
	if (i == 1)
	{
		free(array[0]);
		free(array);
		return ;
	}
	i--;
	while (i >= 0)
	{
		free(array[i]);
		i--;
	}
	free(array);
}
