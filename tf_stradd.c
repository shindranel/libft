/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tf_stradd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 18:27:02 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/19 22:04:07 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*tf_stradd(char *str, char c)
{
	char	*cpy;
	int		len;
	int		i;

	if (!str)
	{
		len = 0;
		str = "";
	}
	else
		len = ft_strlen(str);
	if (!c)
		return (str);
	i = 0;
	if (!(cpy = malloc(sizeof(char) * (len + 2))))
		return (NULL);
	while (str[i])
	{
		cpy[i] = str[i];
		i++;
	}
	cpy[i] = c;
	i++;
	cpy[i] = '\0';
	return (cpy);
}
