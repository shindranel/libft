/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 17:38:28 by adequidt          #+#    #+#             */
/*   Updated: 2018/11/18 18:14:21 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcat(char *dest, const char *src)
{
	int		bob;
	int		len;

	bob = 0;
	len = 0;
	while (dest[bob])
		bob++;
	while (src[len])
	{
		dest[bob + len] = src[len];
		len++;
	}
	dest[bob + len] = '\0';
	return (dest);
}
