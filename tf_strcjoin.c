/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tf_strcjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/06 01:35:08 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/19 22:04:34 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** strjoin prevu pour clean la s1 passee en argument
*/

char	*tf_strcjoin(char *s1, char const *s2)
{
	int		j;
	int		i;
	char	*str;

	if (!s1 || !s2)
		return ((void*)0);
	j = 0;
	i = (ft_strlen(s1) + ft_strlen(s2));
	if (!(str = malloc(sizeof(char) * (i + 1))))
		return (NULL);
	i = 0;
	while (s1[i])
	{
		str[i] = s1[i];
		i++;
	}
	while (s2[j])
	{
		str[i + j] = s2[j];
		j++;
	}
	str[i + j] = '\0';
	free(s1);
	return (str);
}
