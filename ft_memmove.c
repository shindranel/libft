/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/17 18:03:56 by adequidt          #+#    #+#             */
/*   Updated: 2018/12/11 12:54:13 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t			i;
	unsigned char	*ex;
	unsigned char	*ad;

	ex = (unsigned char*)src;
	ad = (unsigned char*)dst;
	i = len;
	if (ex < ad)
	{
		while (i > 0)
		{
			ad[i - 1] = ex[i - 1];
			i--;
		}
	}
	else
	{
		i = 0;
		while (i < len)
		{
			ad[i] = ex[i];
			i++;
		}
	}
	return (dst);
}
