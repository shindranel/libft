/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 20:17:52 by adequidt          #+#    #+#             */
/*   Updated: 2018/12/05 15:26:13 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_exceptions(char c)
{
	if (c == '\n' || c == '\t' || c == '\v' || c == ' ' || c == '\f'
			|| c == '\r')
		return (1);
	else
		return (0);
}

int			ft_atoi(const char *str)
{
	int		i;
	int		n;
	int		nb;

	i = 0;
	n = 0;
	nb = 0;
	while (ft_exceptions(str[i]))
		i++;
	if (str[i] == 45)
		n = 1;
	if (str[i] == 45 || str[i] == 43)
		i++;
	while (str[i] > 47 && str[i] < 58)
	{
		nb *= 10;
		nb += (str[i] - 48);
		i++;
	}
	if (n)
		nb = -nb;
	return (nb);
}
