/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tf_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 17:31:31 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/19 21:48:51 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*tf_memalloc(size_t size)
{
	void	*mem;
	int		i;

	if (!(mem = malloc(size)))
		return (NULL);
	i = (int)size;
	while (i != 0)
	{
		i--;
		((char *)mem)[i] = '\0';
	}
	return (mem);
}
