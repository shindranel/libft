/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 12:47:55 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/19 01:45:12 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putendl(char const *s)
{
	int		bob;

	if (!(s))
		return ;
	bob = 0;
	while (s[bob])
	{
		ft_putchar(s[bob]);
		bob++;
	}
	ft_putchar('\n');
}
