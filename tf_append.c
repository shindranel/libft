/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tf_append.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/27 01:38:19 by adequidt          #+#    #+#             */
/*   Updated: 2019/07/13 23:57:30 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"
#include <stdio.h>

char	**tf_append(char **array, char *str)
{
	int		i;
	char	**new_array;

	i = 0;
	while (array[i])
		i++;
	if (!(new_array = malloc(sizeof(char *) * (i + 2))))
		exit(0);
	i = 0;
	while (array[i])
	{
		new_array[i] = tf_strdup(array[i]);
		i++;
	}
	ft_putendl(array[i]);
	new_array[i] = tf_strdup(str);
	new_array[i + 1] = NULL;
	return (new_array);
}
