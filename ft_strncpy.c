/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 17:08:58 by adequidt          #+#    #+#             */
/*   Updated: 2018/11/21 15:30:11 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dest, const char *src, size_t n)
{
	int		bob;

	bob = 0;
	while (src[bob] && bob < (int)n)
	{
		dest[bob] = src[bob];
		bob++;
	}
	while (bob < (int)n)
	{
		dest[bob] = '\0';
		bob++;
	}
	return (dest);
}
