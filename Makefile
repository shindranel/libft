# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: adequidt <adequidt@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/20 14:50:06 by adequidt          #+#    #+#              #
#    Updated: 2019/11/19 17:27:27 by adequidt         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a
DOT_C = ft_atoi.c ft_bzero.c ft_dice.c ft_isalnum.c ft_isalpha.c ft_isascii.c \
		ft_isdigit.c ft_isprint.c ft_lstadd.c ft_lstdel.c ft_lstdelone.c \
		ft_lstiter.c ft_lstmap.c ft_memccpy.c ft_memchr.c ft_memcmp.c \
		ft_memcpy.c ft_memdel.c ft_memmove.c ft_memset.c ft_partcmp.c \
		ft_putchar.c ft_putchar_fd.c ft_putendl.c ft_putendl_fd.c \
		ft_puterror.c ft_putnbr.c ft_putnbr_fd.c ft_putstr.c ft_putstr_fd.c \
		ft_randint.c ft_strcat.c ft_strchr.c ft_strclr.c ft_strcmp.c \
		ft_strcpy.c ft_strdel.c ft_strequ.c ft_striter.c ft_striteri.c \
		ft_strlcat.c ft_strlen.c ft_strncat.c ft_strncmp.c ft_strncpy.c \
		ft_strnequ.c ft_strnstr.c ft_strrchr.c ft_strstr.c ft_strswitch.c \
		ft_strswitch_full.c ft_tolower.c ft_toupper.c get_next_line.c \
		tf_itoa.c tf_lstclone.c tf_lstnew.c tf_memalloc.c tf_str_get_after.c \
		tf_str_get_until.c tf_stradd.c tf_strcjoin.c tf_strdup.c tf_strjoin.c \
		tf_strmap.c tf_strmapi.c tf_strnew.c tf_strremove.c tf_strrev.c \
		tf_strsplit.c tf_strsub.c tf_strtrim.c ft_array_clean.c tf_append.c\
		tf_stradd_free.c ft_satoi.c
DOT_O = $(DOT_C:.c=.o)

all: $(NAME)

$(NAME): $(DOT_O)
	@ar rc $(NAME) $(DOT_O) libft.h
	@ranlib $(NAME)
	@make clean
	@echo "Compilation libft: ok."

clean:
	rm -f $(DOT_O)

%.o: %.c libft.h
	@gcc -g -o $@ -c $<

fclean: clean
	rm -f $(NAME)
	rm -f libft.h.gch

re: fclean all

.PHONY: all clean fclean re
.SILENT: all clean fclean re
