/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_partcmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/18 23:40:17 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/18 23:53:11 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_partcmp(const char *str, const char *part)
{
	int		i;

	i = 0;
	while (str[i] && part[i] && str[i] == part[i])
		i++;
	if (str[i] && part[i])
		return ((unsigned char)str[i] - (unsigned char)part[i]);
	return (0);
}
