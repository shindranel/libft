/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 17:31:31 by adequidt          #+#    #+#             */
/*   Updated: 2018/12/05 15:34:42 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memalloc(size_t size)
{
	void	*mem;
	int		i;

	if (!(mem = malloc(size)))
		return (NULL);
	i = (int)size;
	while (i != 0)
	{
		i--;
		((char *)mem)[i] = '\0';
	}
	return (mem);
}
