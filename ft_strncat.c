/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 18:15:35 by adequidt          #+#    #+#             */
/*   Updated: 2018/11/18 18:34:53 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	int		bob;
	int		len;
	int		c;

	c = (int)n;
	bob = 0;
	len = 0;
	while (dest[bob])
		bob++;
	while (src[len] && len < c)
	{
		dest[bob + len] = src[len];
		len++;
	}
	dest[bob + len] = '\0';
	return (dest);
}
