/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 15:20:20 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/20 01:00:42 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		find_end(char *str)
{
	while (*str)
	{
		if (*str == '\n')
			return (1);
		str++;
	}
	return (0);
}

void	strcrush(char *src, char *dst)
{
	char	*i;

	i = src;
	while (*src)
	{
		*dst = *src;
		src++;
		dst++;
	}
	*dst = '\0';
	free(src);
}

int		put_in_line(char *str, char **line, int nbr)
{
	char		*tmp;
	int			len;

	len = 0;
	if (nbr < 0)
		return (-1);
	while (str[len] != '\0' && str[len] != '\n')
		len++;
	if (str[len] == '\n')
	{
		*line = tf_strsub(str, 0, len);
		tmp = tf_strdup(str + len + 1);
		ft_strclr(str);
		strcrush(tmp, str);
	}
	else if (str[len] == '\0')
	{
		*line = tf_strdup(str);
		ft_strclr(str);
	}
	return (1);
}

int		get_next_line(const int fd, char **line)
{
	char			buf[BUFF_SIZE + 1];
	char static		*str[1];
	char			*tmp;
	int				nbr;

	if (fd < 0 || line == NULL || read(fd, buf, 0) == -1)
		return (-1);
	while ((nbr = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[nbr] = '\0';
		if (*str == NULL)
			*str = tf_strnew(1);
		tmp = tf_strjoin(*str, buf);
		ft_memdel((void **)str);
		*str = tf_strdup(tmp);
		free(tmp);
		if (find_end(*str) || nbr != BUFF_SIZE)
			break ;
	}
	if (nbr <= 1 && !(ft_strlen(*str)))
		return (0);
	return (put_in_line(*str, line, nbr));
}
