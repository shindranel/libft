/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 19:48:19 by adequidt          #+#    #+#             */
/*   Updated: 2018/11/29 17:45:08 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strnstr(const char *h, const char *n, size_t len)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	if (!(n[i]))
		return ((char *)h);
	while (h[i] && i < (int)len)
	{
		while (h[i + j] == n[j] && (i + j) < (int)len)
		{
			if (!(n[j + 1]))
				return ((char *)(h + i));
			j++;
		}
		j = 0;
		i++;
	}
	return (NULL);
}
