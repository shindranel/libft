/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fr_str_get_until.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/19 01:44:46 by adequidt          #+#    #+#             */
/*   Updated: 2019/06/19 01:44:50 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*fr_str_get_until(char *str, char end)
{
	int		i;
	int		j;
	char	*cpy;

	i = 0;
	j = 0;
	while (str[i] != end && str[i])
	{
		i++;
	}
	if (!(str[i]))
		return (0);
	if (!(cpy = malloc(sizeof(str) * (i + 1))))
		exit(0);
	while (j != i)
	{
		cpy[j] = str[j];
		j++;
	}
	cpy[j] = '\0';
	return (cpy);
}
