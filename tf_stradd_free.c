/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tf_stradd_free.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <adequidt@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/11 18:27:02 by adequidt          #+#    #+#             */
/*   Updated: 2019/11/08 15:40:39 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <stdio.h>

char	*tf_stradd_free(char *str, char c)
{
	char	*cpy;
	int		len;
	int		i;

	if (!str)
		str = "";
	else
		len = ft_strlen(str);
	if (!c)
		return (str);
	i = 0;
	if (!(cpy = malloc(sizeof(char) * (len + 2))))
		return (NULL);
	while (str[i])
	{
		cpy[i] = str[i];
		i++;
	}
	cpy[i] = c;
	i++;
	cpy[i] = '\0';
	free(str);
	return (cpy);
}
