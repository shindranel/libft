/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_satoi.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adequidt <adequidt@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/18 20:17:52 by adequidt          #+#    #+#             */
/*   Updated: 2019/11/19 17:26:57 by adequidt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_exceptions(char c)
{
	if (c == '\n' || c == '\t' || c == '\v' || c == ' ' || c == '\f'
			|| c == '\r')
		return (1);
	else
		return (0);
}

/*
** Secured atoi
*/

int			ft_satoi(const char *str)
{
	int		i;
	int		n;
	int		nb;

	i = 0;
	n = 0;
	nb = 0;
	while (ft_exceptions(str[i]))
		i++;
	if (str[i] == '-')
		n = 1;
	if (str[i] == '-' || str[i] == '+')
		i++;
	while (str[i] >= '0' && str[i] <= '9')
	{
		nb *= 10;
		nb += (str[i] - 48);
		if (nb < 0)
			return(0);
		i++;
	}
	if (n)
		nb = -nb;
	return (nb);
}
